package es.um.ssdd.practica;

import java.io.IOException;
import java.util.logging.Logger;

import es.um.ssdd.practica.database.MongoDBDAOImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;

public class ServiceServer {
    private static final Logger logger = Logger.getLogger(ServiceServer.class.getName());

    /* The port on which the server should run */
    private int port = 50051;
    private Server server;

    private static String mdbHost;
    private static int mdbPort;
    private static String mdbUser;
    private static String mdbPass;

    private static MongoDBDAOImpl mongoClient;

    private void start() throws IOException {
        server = ServerBuilder.forPort(port)
                .addService(new ServiceImpl(logger))
                .build().start();
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown
                // hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                ServiceServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon
     * threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    /**
     * Main launches the server from the command line.
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        final ServiceServer server = new ServiceServer();
        server.initVars();
        server.start();
        server.blockUntilShutdown();
    }

	/**
	 * Inicializar las variables necesarias para la conexion
	 * con la base de datos
	 */
	private void initVars() {
        mdbHost = System.getenv("MDB_HOST");
        mdbUser = System.getenv("MDB_USER");
        mdbPass = System.getenv("MDB_PASS");
        if (mdbHost == null || mdbUser == null || mdbPass == null)
            exit();

        try {
            mdbPort = Integer.parseInt(System.getenv("MDB_PORT"));
        } catch (NumberFormatException e) {
            exit();
        }
        mongoClient = MongoDBDAOImpl.getInstance();
    }

    private void exit() {
        System.out.println("******");
        System.out.println("Variables de entorno no establecidas");
        System.out.println("******");
        System.exit(-1);
    }

    public static MongoDBDAOImpl getMongoClient() { return mongoClient; }
    public static String getMdbHost() { return mdbHost; }
    public static int getMdbPort() { return mdbPort; }
    public static String getMdbUser() { return mdbUser; }
    public static String getMdbPass() { return mdbPass; }

}
