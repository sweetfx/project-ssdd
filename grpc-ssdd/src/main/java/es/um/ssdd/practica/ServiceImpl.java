package es.um.ssdd.practica;

import com.google.protobuf.ByteString;
import es.um.ssdd.practica.model.VideoModel;
import es.um.ssdd.practica.videoservice.*;
import io.grpc.stub.StreamObserver;
import org.bson.types.ObjectId;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ServiceImpl extends VideoServiceGrpc.VideoServiceImplBase {

    private static Logger logger;
    private static String tmpPath = String.format("%s", System.getProperty("java.io.tmpdir"));

    public ServiceImpl(Logger logger) {
        super();
        ServiceImpl.logger = logger;
        logger.info("Path tmp folder: " + tmpPath);
    }

    @Override
    public StreamObserver<Video> uploadVideo(StreamObserver<UploadResponse> responseObserver) {
        final String[] id = {""};
        final String[] ext = {""};
        final String[] lPath = {""};
        final FileOutputStream[] fos = {null};
        return new StreamObserver<Video>() {
            @Override
            public void onCompleted() {
                // Terminar la respuesta
                try {
                    fos[0].close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                responseObserver.onNext(UploadResponse.newBuilder().setId(id[0]).setCode(UploadStatusCode.OK).build());
                responseObserver.onCompleted();
                ServiceServer.getMongoClient().updateVideo(id[0], "status", "procesando");

                // Crear un hilo que procese el vídeo
                Thread t = new Thread() {
                    @Override
                    public synchronized void start() {
                        VideoFaces2 vf2 = null;

                        vf2 = new VideoFaces2(lPath[0], id[0], ext[0]);

                        int cnt = 0;
                        // Obtener las caras del video
                        while (!vf2.isCompleted());
                        ServiceServer.getMongoClient().updateVideo(id[0], "status", "completado");
                        logger.info("Video " + id[0] + " procesado");
                    }
                };

                t.start();
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable arg0) {
                // responseObserver.onError();
                // responseObserver.onCompleted();
            }

            @Override
            public void onNext(Video video) {
                if (id[0].equals("")) {
                    ObjectId objectId = new ObjectId(video.getId());
                    id[0] = objectId.toString();
                    ext[0] = video.getName().split("\\.")[1];
                    lPath[0] = String.format("%s%s%s", tmpPath, System.getProperty("file.separator"), id[0]);
                    createFolder(id[0]);
                    // Generar la entrada del v'ideo en la base de datos
                    ServiceServer.getMongoClient().createVideo(new VideoModel(id[0], video.getName(), video.getIdUploader(), lPath[0]));
                }
                logger.info("Video: " + video.getId() + ". Parte " + video.getPart().getIndex() + ":" + video.getTotalParts());
                // Escribir en el fichero localizado en /tmp/:id/:id
                try {
                    fos[0] = new FileOutputStream(String.format("%s%s%s.%s", lPath[0], System.getProperty("file.separator"), id[0], ext[0]), true);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                try {
                    video.getPart().getContent().writeTo(fos[0]);
                    fos[0].close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public void getImages(VideoSpec request, StreamObserver<ImageResponse> responseObserver) {
        // super.getImages(request, responseObserver);
        String id = request.getId();

        if (!checkFileName(id)) {
            responseObserver.onNext(createMsg(String.valueOf(UploadStatusCode.FAILED), "Petici'on incorrecta"));
            responseObserver.onCompleted();
            return;
        }

        if (ServiceServer.getMongoClient().getStatus(id).toLowerCase().equals("procesando")) {
            responseObserver.onNext(createMsg(String.valueOf(UploadStatusCode.PARTIAL), "Procesando: "+id));
            responseObserver.onCompleted();
            return;
        }

        String path = String.format("%s%s%s%s", System.getProperty("java.io.tmpdir"), System.getProperty("file.separator"), id, System.getProperty("file.separator"));

        // Recorrer directorio y enviar cada imagen
        File f = new File(path);
        try (Stream<Path> walk = Files.walk(Paths.get(path))) {
            List<String> results = walk.map(Path::toString)
                    .filter(fi -> fi.endsWith(".png")).collect(Collectors.toList());

            for (String result : results) {
                String[] aux = result.split("/");
                String imageId = aux[aux.length - 1];
                responseObserver.onNext(ImageResponse.newBuilder()
                        .setId(String.valueOf(UploadStatusCode.OK))
                        .setMessage(id)
                        .setImage(createImageData(imageId, Files.readAllBytes(Paths.get(result))))
                        .build());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        responseObserver.onCompleted();
    }

    /**
     * Crear un directorio para almacenar el video y las imágenes
     * El directorio se crea en /tmp
     *
     * @param id
     * @return
     */
    private boolean createFolder(String id) {
        return (new File((String.format("%s%s%s", tmpPath, System.getProperty("file.separator"), id))).mkdir());
    }

    /**
     * Comprobar si el video existe
     *
     * @param id
     * @return
     */
    private boolean checkFolder(String id) {
        File folder = new File(id);
        return folder.isDirectory();
    }

    private boolean checkFileName(String fileName) {
        return fileName != null && fileName.length() != 0;
    }

    private ImageResponse createMsg(String code, String msg) {
        return ImageResponse.newBuilder()
                .setId(code)
                .setMessage(msg)
                .setImage(ImageData.newBuilder().build()).build();
    }

    private ImageData createImageData(String id, byte[] data) {
        return ImageData.newBuilder()
                .setId(id)
                .setData(ByteString.copyFrom(data))
                .build();
    }

}
