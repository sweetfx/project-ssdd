package es.um.ssdd.practica.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class VideoModel {
    private String path;
    private String id;
    private String vname;
    private String uploadedBy;
    private String dateUpload;

    public VideoModel(String id, String vname, String uploadedBy, String path) {
        this.id = id;
        this.vname = vname;
        this.uploadedBy = uploadedBy;
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        this.dateUpload = df.format(new Date());
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public String getVname() {
        return vname;
    }

    public String getUploadedBy() { return uploadedBy; }

    public String getDateUpload() { return dateUpload; }

    public String getPath() { return path; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VideoModel video = (VideoModel) o;
        return Objects.equals(id, video.id) &&
                Objects.equals(vname, video.vname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vname);
    }
}
