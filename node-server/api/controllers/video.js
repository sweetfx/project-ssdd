'use strict';

var FormData = require('form-data');
var multer  = require('multer');
var upload = multer().any();
const request = require('request');
const mongoose = require('mongoose');
var User = require('../models/user');
var Video = require('../models/video');
const IP = process.env.INTERNAL_API;

exports.uploadVideoInternal = function(req, res){
  User.findById(req.params.id).exec(function (err, userObj) {
    // Si la peticion tiene algun error...
    if (err) {
        console.log(err);
        res.status(500).send({
            error: 'Request error'
        })
    } else if (!userObj) { // Si no encontramos el objeto
        res.status(404).send({
            error: 'Not found'
        })
    } else { // Podemos modificar el objeto antes de devolverlo, por ejemplo, quitando elementos.
      var user = userObj.username;
      upload(req,res,function(err) { 
        if(err) {  
            res.send(err) 
        } 
        else {
    
            //console.log(req.params);
            var files = req.files;
            //console.log(files);
            if (files != ""){
              //console.log(files[0].buffer);
              const url = 'http://'+IP+':8080/internal-api-ssdd/user'
              var reque = request.post(url, function (err, res, body) {
                if (err) {
                  console.log('Error!');
                } else {
                  console.log('URL: ' + body);
                }
              });
              //console.log(url);
              var form = reque.form();
              
              form.append('file', files[0].buffer, {
                filename: files[0].originalname,
                contentType: 'multipart/form-data' 
              });
              // Sustituir el host por la dirección donde se aloje el servidor Tomcat
              // Sustituir el nombre de usuario por el que me llegue(buscar id en la bbdd)
              form.submit({
                host: IP,
                port: '8080',
                path: '/internal-api-ssdd/user',
                file: files[0].buffer,
                headers: {'user': user}
              }, function(err, res) {
                console.log(res.statusCode);
                
              });
            }
        }
      });
    }
});
}

exports.getImages = function(req, res){
  User.findById(req.params.id).exec(function (err, userObj) {
    // Si la peticion tiene algun error...
    if (err) {
        console.log(err);
        res.status(500).send({
            error: 'Request error'
        })
    } else if (!userObj) { // Si no encontramos el objeto
        res.status(404).send({
            error: 'Not found'
        })
    } else { // Podemos modificar el objeto antes de devolverlo, por ejemplo, quitando elementos.
        var video = req.params.idVideo;
       
        Video.findById(video).exec(function (err, videObj) {
          // Si la peticion tiene algun error...
          if (err) {
              console.log(err);
              res.status(500).send({
                  error: 'Request error'
              })
          } else if (!videObj) { // Si no encontramos el objeto
              res.status(404).send({
                  error: 'Not found'
              })
          } else { // Podemos modificar el objeto antes de devolverlo, por ejemplo, quitando elementos.
            if(videObj.status !== 'completado'){
              res.status(204).send('The video is still processing. Try it again in other moment');
            }else{
              var newvalue = { $inc: { views: 1 } };
              Video.findOneAndUpdate({_id:video}, newvalue, {useFindAndModify: false}, function(err, res) {
                if (err) throw err;
              });
              var inc = videObj.views;
              inc+=1;
              var info = "Title: " + videObj.title + ", date: " + videObj.date + ", views: " + inc + ", faces: " + videObj.faces;
              var faces = " ";
              videObj.images.forEach(element => { 
                faces = faces + element.name + " " + element._id + " " + element.content.toString('base64') + " ";
              });
              return res.render('video',{data:{str:info,photos:faces}},function(err,html){
                res.send(html);
              });
            }

          }
      });
    }
  });
}

exports.deleteImages = function (req, res) {
	var video = req.params.idVideo;
	var image = req.body.imageid;
	
	Video.findByIdAndUpdate(video, {$pull: {"images": {_id: mongoose.Types.ObjectId(image)}}}, { safe: true, upsert: true}, function(err, videObj) {
        if (err) { 
			console.log(err);
            res.status(500).send({
                error: 'Server error'
            }) 
		}else if (!videObj) { // Si no encontramos el objeto
            res.status(404).send({
                error: 'Not found'
			})
		}
		Video.findByIdAndUpdate(video, {$inc:{faces: -1}}, { safe: true, upsert: true}, function(err, res) {
			if (err) throw err;
		});
        return res.status(200).json(videObj.images);
    });
}

exports.deleteVideo = function (req, res) {
	var user = req.params.id;
	var video = req.body.videoid;
	
	Video.findByIdAndDelete(video, function(err, output){
		if (err){
			console.log(err);
            res.status(500).send({
                error: 'Server error'
            })
		}
	});
  
	User.findByIdAndUpdate(user, {$pull: {"videos": {_id: mongoose.Types.ObjectId(video)}}}, { safe: true, upsert: true}, function(err, userObj) {
        if (err) { 
			console.log(err);
            res.status(500).send({
                error: 'Server error'
            }) 
		}else if (!userObj) { // Si no encontramos el objeto
            res.status(404).send({
                error: 'User not found'
			})
		}
        return res.status(200).json(userObj.videos);
    });
}
