// Utilizar funcionalidades del Ecmascript 6
'use strict'

// Cargamos los módulos de express y body-parser
var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require("method-override");

// Llamamos a express para poder crear el servidor
var app = express();

// Importamos las rutas
var index_route = require('./routes/index');
var user_route = require('./routes/users');
var login_route = require('./routes/login');
var developer_route = require('./routes/developers');

//un metodo que se ejecuta antes que llegue a un controlador
//Configuramos bodyParser para que convierta el body de nuestras peticiones a JSON
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(methodOverride());

app.engine('html',require('ejs').renderFile);
app.set('view engine','html');
app.use(express.static(__dirname + '/public'));

// Cargamos las rutas
app.use('/', index_route);
app.use('/api', user_route);
app.use('/api', login_route);
app.use('/api',developer_route);


// exportamos este módulo para poder usar la variable app fuera de este archivo
module.exports = app;

