"use strict"; 


// Cargamos el módulo de mongoose
var mongoose = require("mongoose"); 
// Usaremos los esquemas
var Schema = mongoose.Schema;

// Creamos el objeto del esquema y sus atributos
var VideoSchema = Schema({
  title: String,
  date: String,
  uploadedBy: String,
  path: String,
  status: String,
  faces: Number,
  views: Number,
  images: [{}]
}); 

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model("Video", VideoSchema);