'use strict'

// Cargamos el módulo de express para poder crear rutas
var express = require('express');

// Cargamos los controladores
var UserController = require('../controllers/user');
var VideoController = require('../controllers/video');

// Llamamos al router
var api = express.Router();

// Creamos una ruta para los métodos que tenemos en nuestros controladores
// Esta ruta recibe un parametro ID y es necesario que vaya autenticada
api.get('/user/:id', UserController.getUser);
// Esta ruta recibe un video que pertenece a un usuario, y lo envía
api.post('/user/:id/video-internal', VideoController.uploadVideoInternal);
api.get('/user/:id/video/:idVideo', VideoController.getImages);
api.post('/user/:id/video/:idVideo/delete', VideoController.deleteImages);
api.post('/user/:id/video/delete', VideoController.deleteVideo);
api.post('/user', UserController.registerUser);

// Exportamos la configuración
module.exports = api;