API Externa para el servicio GRPC
------
Esta API permite utilizar el servicio de detección de caras sin necesidad de pasar por la aplicación web.
Para poder acceder al servicio, es necesario registrarse en el servicio.

## Variables de entorno
Es necesario declarar una serie de variables de entorno que nos van a permitir conectar tanto con el servidor GRPC como la base de datos MongoDB. Sin estas variables, el servicio no se ejecutará correctamente.

Las siguientes variables son necesarias para la conexión con el servidor GRPC.

Variable | Valor | Descripción
--- | --- | ---
GRPC_HOST | *localhost* | dirección del servidor GRPC.
GRPC_PORT | *50051* | puerto donde escucha el servidor GRPC.

Y las variables para conectar con la base de datos, en nuestro caso, MongoDB.

Variable | Valor | Descripción
--- | --- | ---
MDB_HOST | *localhost* | dirección de la base datos.
MDB_PORT | *27017* | puerto donde escucha la base de datos Mongo.
MDB_USER | *user* | usuario utilizado para conectar a la base de datos.
MDB_PASS | *pass* | contraseña del usuario

## Pasos para el despliegue del servicio
1. Necesario descargar el proyecto **grpc-ssdd** y generar el .jar del cliente. Para esto, nos situamos dentro del proyecto importado y realizamos las siguientes acciones:
   1. Realizamos ```mvn clean install```.
   2. Generamos el .jar necesario con ```mvn package```. Esto nos genera dos .jar, uno del cliente y otro del servidor, ambos contienen las dependencias necesarias. Situados están en la carpeta **target**.
   3. Instalamos el cliente en el repositorio local de **maven**
    > $ mvn install:install-file \                
    >  -Dfile=target/grpc-client-jar-with-dependencies.jar \
    >  -DgroupId=es.um.ssdd.grpc \
    >  -DartifactId=grpc-client \
    >  -Dversion=0.0.1-SNAPSHOT \
    >  -Dpackaging=jar
2. Una vez instalado el cliente grpc, ```mvn clean install``` en el directorio de la api rest para generar los ficheros del proyecto.
3. Desplegar el .war en un servidor Tomcat, o abrir el proyecto con Eclipse/Intellij Idea y desplegarlo desde ahí.

-----------
## Endpoints

### Registro de usuario
> POST ```/video/user/register```

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
username   | Nombre del usuario
password   | Contraseña del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
400 |  | Si falta algún campo
200 | User exists | El nombre del usuario ya existe
200 | Registered  | El usuario ha sido registrado correctamente

### Subir vídeo
> POST ```/video```

#### Cabeceras de la petición
Cabecera | Valor | Descripción
--- | --- | ---
User | String | Nombre del usuario
Date | String |Fecha en formato ISO8601, segundos y milisegundos a 0 (:00.000)
Auth-Token | String |Token generado en la parte del cliente para identificar al usuario.
Content-Type | multipart/form-data | Tipo de contenido incluido en la petición 

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
username   | Nombre del usuario
password   | Contraseña del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
202 | {:id} | El vídeo ha sido subido correctamente.
400 | Bad headers  | Si falta alguna cabecera
400 | Check filename | Comprobar el fichero subido
401 | You dont have permission  | El usuario no tiene permisos para acceder al servicio (Comprobar token de cliente)
404 | Wrong user or password | Usuario o contraseña incorrecta
500 | Error uploading file. Please, try again | El servidor GRPC está fallando
503 | Service GRPC unavailable | El servidor GRPC no está disponible
504 | Error uploading file to grpc | El servidor GRPC ha fallado al subir el fichero 

### Obtner imágenes
> GET ```/video/:id```

#### Cabeceras de la petición
Cabecera | Valor | Descripción
--- | --- | ---
User | String | Nombre del usuario
Date | String |Fecha en formato ISO8601, segundos y milisegundos a 0 (:00.000)
Token | String |Token generado en la parte del cliente para identificar al usuario.
Content-Type | application | application/

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
username   | Nombre del usuario
password   | Contraseña del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
200 |  | El vídeo ha sido procesado. El cuerpo del mensaje incluye todas las imágenes encontradas.
204 |  | El vídeo está siendo procesado
400 | Bad headers  | Si falta alguna cabecera
400 | Check filename | Comprobar el fichero subido
401 | You dont have permission  | El usuario no tiene permisos para acceder al servicio (Comprobar token de cliente)
404 | Wrong user or password | Usuario o contraseña incorrecta
500 | Error uploading file. Please, try again | El servidor GRPC está fallando
503 | Service GRPC unavailable | El servidor GRPC no está disponible
504 | Error uploading file to grpc | El servidor GRPC ha fallado al subir el fichero 