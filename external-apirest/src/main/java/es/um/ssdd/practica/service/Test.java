package es.um.ssdd.practica.service;

import es.um.ssdd.practica.model.Video;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/test")
public class Test {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVideo() {
        Video video = new Video("asdasdasd", "peazo_video.mp4");
        GenericEntity<Video> entity = new GenericEntity<Video>(video){};
        return Response.status(200).entity(entity).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response postVideo() {
        Video video = new Video("posted", "peazo_video.mp4");
        GenericEntity<Video> entity = new GenericEntity<Video>(video){};
        return Response.status(200).entity(entity).build();
    }
}
