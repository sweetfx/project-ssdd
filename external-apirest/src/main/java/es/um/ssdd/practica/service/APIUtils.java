package es.um.ssdd.practica.service;

import javax.ws.rs.core.HttpHeaders;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

class APIUtils {
    protected static final String TMPPATH = String.format("%s%s%s%s", System.getProperty("java.io.tmpdir"), System.getProperty("file.separator"), "apirest", System.getProperty("file.separator"));

    private static boolean createTmpApi() {
        File f = new File(TMPPATH);
        if (!f.exists())
            f.mkdir();
        return f.exists();
    }

    protected static String writeTmpFile(String fileData[], InputStream content) throws IOException{
        if (!createTmpApi())
            throw new IOException();

        String id = generateMD5(fileData[0]);
        String ext = fileData[1];
        String path = createFolder(id);
        try {
            FileOutputStream fos = new FileOutputStream(String.format("%s%s%s.%s", path, System.getProperty("file.separator"), id, ext));
            int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = content.read(bytes)) != -1) {
				fos.write(bytes, 0, read);
			}

            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return id;
    }

    private static String createFolder(String id) {
        File folder = new File((String.format("%s%s%s", TMPPATH, System.getProperty("file.separator"), id)));
        folder.mkdir();
        return folder.getAbsolutePath();
    }

    protected static String[] extractFileData(String fileName) {
        String[] fileData = new String[2];
        try {
            fileData[0] = fileName.split("\\.")[0];
            fileData[1] = fileName.split("\\.")[1];
        } catch (NullPointerException e) {
            fileData = null;
            e.printStackTrace();
        }
        return fileData;
    }

    protected static String generateToken(String userPass, String path) {
        // Conversion
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:00.000'Z'");
        String dateText = sdf.format((new GregorianCalendar()).getTime());
        String token = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            token = path.toString() + dateText + userPass;
            byte[] messageDigest = md.digest(token.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);

            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String generateMD5(String id) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            id = id + new Timestamp((new Date()).getTime());
            byte[] messageDigest = md.digest(id.getBytes());

            BigInteger no = new BigInteger(1, messageDigest);

            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return id;
    }

    protected static boolean checkHeaders(HttpHeaders headers) {
        if ((headers.getRequestHeader("user") == null ) || (headers.getRequestHeader("date") == null ) || (headers.getRequestHeader("auth-token") == null ) )
            return false;
        return true;
    }
}
