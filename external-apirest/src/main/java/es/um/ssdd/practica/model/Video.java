package es.um.ssdd.practica.model;

import java.util.Objects;

public class Video {
    private String id;
    private String vname;

    public Video() {};

    public Video(String id, String vname) {
        this.id = id;
        this.vname= vname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Video video = (Video) o;
        return Objects.equals(id, video.id) &&
                Objects.equals(vname, video.vname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vname);
    }
}
