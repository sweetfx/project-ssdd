package es.um.ssdd.practica.model;

import org.mindrot.jbcrypt.BCrypt;

import java.util.List;
import java.util.Objects;

public class User {
    private String username;
    private String password;
    private List<String> videos;

    public User() {};

    public User(String username, String password) {
        this.username = username;
        this.password = encryptPassword(password);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getVideos() {
        return videos;
    }

    public void addVideo(String videoId) {
        this.videos.add(videoId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(videos, user.videos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, videos);
    }

    private String encryptPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(12)).replace("$2a$", "$2y$");
    }
}
