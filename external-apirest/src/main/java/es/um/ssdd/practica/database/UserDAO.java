package es.um.ssdd.practica.database;

import es.um.ssdd.practica.model.User;

import java.util.List;

public interface UserDAO {
    boolean createUser(User user);
    User getUser(String username);
    void updateUser(String username, String idVideo, String videoName);
}
