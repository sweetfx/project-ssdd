package es.um.ssdd.practica;

import es.um.ssdd.practica.database.MongoDBDAOImpl;
import es.um.ssdd.practica.database.UserDAO;
import org.glassfish.jersey.server.ResourceConfig;

public class AppConfig extends ResourceConfig {

    private static String grpcHost;
    private static int grpcPort;
    private static String mdbHost;
    private static int mdbPort;
    private static String mdbUser;
    private static String mdbPass;

    private static UserDAO mongoClient;

    public AppConfig() {
        grpcHost = System.getenv("GRPC_HOST");
        mdbHost = System.getenv("MDB_HOST");
        mdbUser = System.getenv("MDB_USER");
        mdbPass = System.getenv("MDB_PASS");
        if (grpcHost == null || mdbHost == null || mdbUser == null || mdbPass == null)
            exit();

        try {
            mdbPort = Integer.parseInt(System.getenv("MDB_PORT"));
            grpcPort = Integer.parseInt(System.getenv("GRPC_PORT"));
        } catch (NumberFormatException e) {
            exit();
        }
        mongoClient = MongoDBDAOImpl.getInstance();
    }

    private void exit() {
        System.out.println("******");
        System.out.println("Variables de entorno no establecidas");
        System.out.println("******");
        System.exit(-1);
    }

    public static UserDAO getMongoClient() {
        return mongoClient;
    }

    public static String getGrpcHost() {
        return grpcHost;
    }

    public static int getGrpcPort() {
        return grpcPort;
    }

    public static String getMdbHost() {
        return mdbHost;
    }

    public static int getMdbPort() {
        return mdbPort;
    }

    public static String getMdbUser() {
        return mdbUser;
    }

    public static String getMdbPass() {
        return mdbPass;
    }
}
